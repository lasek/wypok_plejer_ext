function szukaj_chujowego_odtwarzacza_kurwa() {
    return document.querySelectorAll('iframe[src^="https://www.youtube"][src*=enablejsapi]');
}

function wyczysc_źródło_filmu_kurwa(źródło_filmu) {
    return źródło_filmu.replace(/\?.+$/, '');
}

function usuń_ścierwo_chujowego_plejera() {
    var elementy_do_wyjebania = document.querySelectorAll([
        '.jw-preview',
        '.jw-title',
        '.jw-captions',
        '.jw-overlays',
        '.jw-controls',
    ].join(','));

    for (var i = 0; i < elementy_do_wyjebania.length; i++) {
        var element_do_wyjebania = elementy_do_wyjebania[i]

        element_do_wyjebania.parentNode.removeChild(element_do_wyjebania);
    }
}

function podmien_chujowy_odtwarzacz() {
    var chujowe_odtwarzacze_kurwa = szukaj_chujowego_odtwarzacza_kurwa();

    for (var i = 0; i < chujowe_odtwarzacze_kurwa.length; i++) {
        var chujowy_odtwarzacz_kurwa = chujowe_odtwarzacze_kurwa[i]
        var źródło_filmu = chujowy_odtwarzacz_kurwa.src;

        chujowy_odtwarzacz_kurwa.src = wyczysc_źródło_filmu_kurwa(źródło_filmu);
    }

    usuń_ścierwo_chujowego_plejera();
}

document.addEventListener('DOMNodeInserted', podmien_chujowy_odtwarzacz, true);
document.addEventListener('DOMSubtreeModified', podmien_chujowy_odtwarzacz, true);
